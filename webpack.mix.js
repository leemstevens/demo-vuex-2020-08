const mix = require('laravel-mix');

mix
    // .combine([
    //     'node_modules/jquery/dist/jquery.min.js',
    // ], 'public/js/vendor.js')

    .js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
// .js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');

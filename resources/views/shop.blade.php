@extends('layouts.app')

@section('content')
    <div class="center-frame">
        <div class="top-title">
            <h1>Kittenz 4 dayz</h1>
        </div>
        <kitteh-shop></kitteh-shop>
        <pop-cart></pop-cart>
    </div>
@endsection
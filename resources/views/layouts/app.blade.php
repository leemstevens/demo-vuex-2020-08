<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts.partials.html-head', [
            'cssFile'=>'app',
        ])
    </head>
    <body id="route-{{ (Request::route()) ? str_replace('.', '-', \Request::route()->getName()) : 'null' }}" class="layout-admin">
        <div id="vue-app">
            {{-- @include ('common.components.top-bar') --}}
            <main id="page-main">
                @yield('content')
            </main>
            {{-- @include ('layouts.partials.admin-footer') --}}
        </div>
        @include ('layouts.partials.body-end', [
            'jsDir'=>'',
        ])
    </body>
</html>
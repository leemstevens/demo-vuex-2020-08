

import axios from 'axios';

const state = {
    kittehs: [],
    kittehsById: {},
};

const getters = {
    allKittehs: (state) => state.kittehs,
    kittehsById: (state) => state.kittehsById,
    kittehQty: (state, getters, rootState) => (id) => {
        let cartKitteh = rootState.storeCart.cartKittehs.find(cartKitteh => {
            return cartKitteh.kitten_id === id;
        });
        if (cartKitteh) {
            return cartKitteh.qty;
        } else {
            return 0;
        }
    },
};

const actions = {

    async fetchKittehs({ commit }) {
        const response = await axios.get('/ajax/kittehs/all');
        commit('setKittehs', response.data);
    },

    async adjustKittehQty({ commit, rootState, dispatch }, { id, inc }) {
        const response = await axios.post('/ajax/cart/update-kitteh', {
            kitten_id: id,
            inc
        });

        let cartKitteh = response.data.cartKitteh,
            qty = cartKitteh.qty;
        
        dispatch('updateCartKitteh', {cartKitteh, qty});

        
        // let cartKittehId = response.data.cartKitteh.id,
        //     qty = response.data.cartKitteh.qty,
        //     cartKittehExists = typeof rootState.storeCart.cartKittehsById[cartKittehId] !== 'undefined';

        // if (qty > 0) {
        //     if (cartKittehExists) {
        //         commit('updateCartKittehQty', {cartKittehId, qty});
        //     } else {
        //         commit('setCartKittehs', rootState.storeCart.cartKittehs.concat([response.data.cartKitteh]));
        //     }
        // } else {
        //     if (cartKittehExists) {
        //         commit('setCartKittehs', rootState.storeCart.cartKittehs.filter(cartKitteh => {
        //             return cartKitteh.id !== cartKittehId;
        //         }));
        //     }
        // }

    },
};

const mutations = {
    setKittehs: (state, kittehs) => {
        state.kittehs = kittehs;
        kittehs.forEach(kitteh => {
            state.kittehsById[kitteh.id] = kitteh;
        });
    },
};


export default {
    state,
    getters,
    actions,
    mutations,
};

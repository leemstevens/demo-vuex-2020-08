

import axios from 'axios';

const state = {
    cartKittehs: [],
    cartKittehsById: {},
};

const getters = {
    allCartKittehs: (state) => state.cartKittehs,
};

const actions = {

    async fetchCartKittehs({ commit }) {
        const response = await axios.get('/ajax/cart/get');
        commit('setCartKittehs', response.data.cartKittehs);
    },

    async adjustQty({ dispatch }, {cartKittehId, inc}) {
        const response = await axios.post('/ajax/cart/update-cart-kitteh', {
            cart_kitten_id: cartKittehId,
            inc
        });
        let cartKitteh = response.data.cartKitteh,
            qty = cartKitteh.qty;
        console.log(cartKittehId, cartKitteh);
        dispatch('updateCartKitteh', {cartKitteh, qty});
    },

    updateCartKitteh({ state, commit }, {cartKitteh, qty}) {
        let cartKittehExists = typeof state.cartKittehsById[cartKitteh.id] !== 'undefined';
        if (qty < 0) { qty = 0; }
        if (qty > 0) {
            if (cartKittehExists) {
                commit('updateCartKittehQty', {cartKittehId: cartKitteh.id, qty});
            } else {
                commit('setCartKittehs', state.cartKittehs.concat([cartKitteh]));
            }
        } else {
            if (cartKittehExists) {
                commit('setCartKittehs', state.cartKittehs.filter(aCartKitteh => {
                    return aCartKitteh.id !== cartKitteh.id;
                }));
            }
        }
    },
};

const mutations = {

    setCartKittehs: (state, cartKittehs) => {
        state.cartKittehs = cartKittehs;
        cartKittehs.forEach(cartKitteh => {
            state.cartKittehsById[cartKitteh.id] = cartKitteh;
        });
    },

    updateCartKittehQty: (state, {cartKittehId, qty}) => {
        state.cartKittehsById[cartKittehId].qty = qty;
    },
};


export default {
    state,
    getters,
    actions,
    mutations,
};

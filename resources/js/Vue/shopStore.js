
import axios from 'axios';

const state = {
    kittehs: [],
};

const getters = {
    fetchKittehs: (state) => state.kittehs,
};

const actions = {
    async fetchKittehs({ commit }) {
        const response = await axios.get('/ajax/kittehs/all');

        console.log('shopStore action setKittehs()', response.data);
        commit('setKittehs', response.data);
    }
};

const mutations = {
    setKittehs: (state, kittehs) => (state.kittehs = kittehs),
};


export default {
    state,
    getters,
    actions,
    mutations,
};

require('./bootstrap');


import Vuex from 'vuex'
import storeKitteh from './Vue/store/kittehs.js'
import storeCart from './Vue/store/cart.js'

Vue.use(Vuex);


Vue.component('kitteh-shop', () => import('./Vue/KittehShop.vue'));
Vue.component('pop-cart', () => import('./Vue/PopCart.vue'));

const vueApp = new Vue({
    el: '#vue-app',
    store: new Vuex.Store({
        modules: {
            storeKitteh,
            storeCart,
        },
    }),
});

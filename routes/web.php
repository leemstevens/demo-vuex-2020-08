<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('shop');
});


Route::get('/ajax/kittehs/all', 'KittenController@ajaxAll')->name('ajax-all-kittehs');
Route::get('/ajax/cart/get', 'CartController@ajaxGet')->name('ajax-cart-get');

Route::post('/ajax/cart/update-kitteh', 'CartController@ajaxUpdateKitten')->name('ajax-cart-update-kitten');
Route::post('/ajax/cart/update-cart-kitteh', 'CartController@ajaxUpdateCartKitten')->name('ajax-cart-update-cart-kitten');




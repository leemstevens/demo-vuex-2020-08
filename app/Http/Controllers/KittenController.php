<?php

namespace App\Http\Controllers;

use App\Models\Kitten;
use Illuminate\Http\Request;

class KittenController extends Controller
{

    public static function ajaxAll()
    {
        return response()->json(Kitten::all());
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CartKitten;
use App\Models\Kitten;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function ajaxGet()
    {
        $kittensCart = CartKitten::all();
        return response()->json([
            'cartKittehs' => $kittensCart->toArray(),
        ]);
    }

    public function ajaxUpdateKitten()
    {
        $kittenId = request('kitten_id');
        $inc = request('inc');

        $cartKitten = CartKitten::where('kitten_id', '=', $kittenId)->first();

        if ($cartKitten) {
            $qty = $cartKitten->qty;
        } else {
            $cartKitten = CartKitten::create([
                'cart_id' => 1,
                'kitten_id' => $kittenId,
                'qty' => 0,
            ]);
            $qty = 0;
        }
        $qty += $inc;
        if ($qty < 0) {
            $qty = 0;
        }
        $cartKitten->qty = $qty;

        if ($cartKitten->qty === 0) {
            $cartKitten->delete();
        } else {
            $cartKitten->save();
        }
        
        return response()->json([
            'cartKitteh' => $cartKitten,
        ]);
    }

    public function ajaxUpdateCartKitten()
    {
        $cartKittenId = request('cart_kitten_id');
        $inc = request('inc');

        $cartKitten = CartKitten::where('id', '=', $cartKittenId)->first();

        if ($cartKitten) {
            $cartKitten->qty += $inc;
            if ($cartKitten->qty < 0) {
                $cartKitten->qty = 0;
            }
        } else {
            abort(404, "cartKitten $cartKittenId not found");
        }

        if ($cartKitten->qty === 0) {
            $cartKitten->delete();
        } else {
            $cartKitten->save();
        }
        
        return response()->json([
            '$cartKittenId' => $cartKittenId,
            'cartKitteh' => $cartKitten,
        ]);
    }
}

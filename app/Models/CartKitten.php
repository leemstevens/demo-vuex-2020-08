<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartKitten extends Model
{
    protected $guarded = [];

    function cart()
    {
        return $this->belongsTo(Cart::class);
    }
    function kitten()
    {
        return $this->belongsTo(Kitten::class);
    }
}

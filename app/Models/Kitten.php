<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kitten extends Model
{
    protected $guarded = [];

    function CartKittens()
    {
        return $this->hasMany(CartKittens::class);
    }
}

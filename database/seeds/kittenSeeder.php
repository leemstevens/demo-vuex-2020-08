<?php

use App\Models\Kitten;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class kittenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $desc = 'I quickly packed veal, quince jam, and for the zoo. Jim quit, vexed, he packed extra bags for Liz Owen. The fox jumps over the dog.';
        DB::table('kittens')->insert([
            'name' => 'Ginger',
            'img_sm' => '/img/kittehs/ginger01-sm.jpg',
            'img_lg' => '/img/kittehs/ginger01-lg.jpg',
            'price' => '7900',
            'description' => $desc,
        ]);
        DB::table('kittens')->insert([
            'name' => 'Tabby Ball',
            'img_sm' => '/img/kittehs/tabby-ball-sm.jpg',
            'img_lg' => '/img/kittehs/tabby-ball-sm.jpg',
            'price' => '3999',
            'description' => $desc,
        ]);
        DB::table('kittens')->insert([
            'name' => 'Professor FuzzyFace',
            'img_sm' => '/img/kittehs/professor-fuzzyface-sm.jpg',
            'img_lg' => '/img/kittehs/professor-fuzzyface-sm.jpg',
            'price' => '4999',
            'description' => $desc,
        ]);
        DB::table('kittens')->insert([
            'name' => 'Cat Stevens',
            'img_sm' => '/img/kittehs/tabby-face-sm.jpg',
            'img_lg' => '/img/kittehs/tabby-face-sm.jpg',
            'price' => '5999',
            'description' => $desc,
        ]);
        DB::table('kittens')->insert([
            'name' => 'Mr Kitten',
            'img_sm' => '/img/kittehs/tabby01-sm.jpg',
            'img_lg' => '/img/kittehs/tabby01-lg.jpg',
            'price' => '1200',
            'description' => $desc,
        ]);
        DB::table('kittens')->insert([
            'name' => 'Wavezies',
            'img_sm' => '/img/kittehs/wavsies.jpg',
            'img_lg' => '/img/kittehs/wavsies.jpg',
            'price' => '999',
            'description' => $desc,
        ]);

        DB::table('cart')->insert([]);
    }
}
